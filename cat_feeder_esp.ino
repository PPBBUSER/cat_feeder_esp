
// Eric Ayars DS3231 library with JeeLabs/Ladyada's RTC libraries spliced in byAndy Wickert
#include <DS3231.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <EEPROM.h>

// u8g2 lib requires patch adding yield() in u8x8_debounce.c in function u8x8_GetMenuEvent(u8x8_t *u8x8), 
// at the beginnig just after pin_state = u8x8_read_pin_state(u8x8) -> not to kick esp8266 watchdog and allow
// bgd services to run while in menu
//
// uint8_t u8x8_GetMenuEvent(u8x8_t *u8x8)
// {
//   uint8_t pin_state;
//   uint8_t result_msg = 0;       /* invalid message, no event */
//
//   pin_state = u8x8_read_pin_state(u8x8);
//   yield();
#include <U8g2lib.h>
#include <Servo.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif


 

// button defines
#define RED_BTN D5
#define R_BTN D5
#define WHITE_BTN D6
#define W_BTN D6
#define BLUE_BTN D7
#define B_BTN D7

// servo, orange wire
#define SERVO_PIN D4 

//EEPROM ADDRESS
#define EEPROM_ADDRESS_FEED_H 0x57
#define EEPROM_ADDRESS_FEED_M 0x58
#define EEPROM_ADDRESS_FEED_D 0x59

// main menu position defines, and item texts
#define MP_FEED 1
#define MP_SET_CLOCK 2
#define MP_SET_TIME 3
#define MP_SET_DOSE 4
#define MP_EXIT 5
#define MENU_POSITIONS "Feed now!\nSet clock\nSet time\nSet dose\nExit"

Servo myservo;

// aid for mechanical display placement debugging ;)
//#define MECHANICAL_DEBUG 1

//#define USE_WIFI 1
//#define WIFI_DEBUG 1

DS3231 Clock;

U8G2_SSD1306_64X48_ER_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);   // EastRising 0.66" OLED breakout board, Uno: A4=SDA, A5=SCL, 5V powered

ESP8266WiFiMulti WiFiMulti;

void initWifi() {
#ifdef USE_WIFI  
#ifdef WIFI_DEBUG
   Serial.print("Connecting... ");
   
#endif   
  while (WiFiMulti.run()!= WL_CONNECTED) {
  delay(10);    
#ifdef WIFI_DEBUG
      Serial.print(".");
#endif      
   }
#ifdef WIFI_DEBUG   
  Serial.print("\nWiFi connected, IP address: ");
  Serial.println(WiFi.localIP());
#endif
#endif
} 

#define DISP_W 64
#define DISP_H 48

/* GLOBALS */

// feed time and wall  time
uint8_t feed_h, feed_m;
uint8_t now_h, now_m, now_s;
uint8_t feed_duration; // in 10ms unit (1==10ms)?

// main menu cursor position
uint8_t menu_pos = 1;


void oled_setup(void) {
  u8g2.setFont(u8g2_font_6x10_tf);
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);
  u8g2.clearBuffer();
  u8g2.sendBuffer();     
}

void oled_print_centered(char * s, int y) 
{
   int w = u8g2.getStrWidth(s);   
   u8g2.drawStr(DISP_W/2-w/2,y,s);
}

int time_diff_in_m(int now_m, int now_h, int feed_h, int feed_m)
{
  int diff = 0;  
  int now_min = now_m+60*now_h;
  int feed_min = feed_m+60*feed_h;  

  if (now_min<=feed_min) { //before
    diff = feed_min - now_min;
  } else {
    diff = feed_min + 24*60 - now_min;
  }
  return diff; 
}

// this shows menu allowing setting of hour and minute
void set_time_helper(char *menu_title, uint8_t *h, uint8_t *m)
{
  bool done = false;
  uint8_t setting = 0;
  
  while (!done) {
    setting = u8g2.userInterfaceSelectionList(menu_title, setting, "Set hour\nSet minute\nExit");
    switch(setting) {
      case 1:
        u8g2.userInterfaceInputValue("Select\n hour", "h: ", h, 0, 23, 3, "");
        setting = 2;
        break;
      case 2:
        u8g2.userInterfaceInputValue("Select\n minute", "m: ", m, 0, 59, 3, "");              
        setting = 3;
        break;
      case 3: // intentionally no break
      default:
        done = true;
        break;
    }
  }
}

void read_hw_clock(void)
{
  bool h12;
  bool PM;
  now_h = Clock.getHour(h12, PM);
  if (now_h>24) now_h = 0;
  now_m = Clock.getMinute();
  if (now_m>60) now_m = 0;
  now_s = Clock.getSecond();
  if (now_s>60) now_s = 0;
}

void draw_main_screen(void)
{
  char str_tmp[32];
  
  u8g2.setFont(u8g2_font_profont17_mf);
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);
  u8g2.clearBuffer();   
#ifdef MECHANICAL_DEBUG
   u8g2.drawFrame(0,0, DISP_W, DISP_H);
#endif   
  
  sprintf(str_tmp,"%02d%c%02d", now_h, (0==now_s%2)?':':' ', now_m);
  oled_print_centered(str_tmp, 2);
  u8g2.setFont(u8g2_font_6x10_tf);
  sprintf(str_tmp,"ETA %dm", time_diff_in_m(now_m, now_h, feed_h, feed_m));   
  oled_print_centered(str_tmp, 2+20);
  sprintf(str_tmp,"Feed %02d:%02d", feed_h, feed_m);   
  oled_print_centered(str_tmp, 2+20+12);   
  u8g2.sendBuffer();
}

void feed(void) {
  Serial.print("Feeding!");
  servo_cw();
  delay(feed_duration*100);
  servo_stop();
}


void set_feed_time() {  
  EEPROM.begin(512);
  /*
  Serial.println("=== setting feed time ==");
  Serial.println(feed_h, DEC);
  Serial.println(feed_m, DEC);
  Serial.println("========================");  */
  EEPROM.write(EEPROM_ADDRESS_FEED_H, feed_h);
  EEPROM.write(EEPROM_ADDRESS_FEED_M, feed_m);
  EEPROM.write(EEPROM_ADDRESS_FEED_D, feed_duration);
  EEPROM.end();
}


void get_feed_time() {
  EEPROM.begin(512);
  feed_h = EEPROM.read(EEPROM_ADDRESS_FEED_H);
  feed_m = EEPROM.read(EEPROM_ADDRESS_FEED_M);
  feed_duration = EEPROM.read(EEPROM_ADDRESS_FEED_D);  
  /*
  Serial.println("=== read feed time  ===");
  Serial.println(feed_h, DEC);
  Serial.println(feed_m, DEC);
  Serial.println("========================");  */

  // safe defaults in case we read back insane values
  if (feed_h > 23) {
    feed_h = 22;
    feed_duration = 20;
  }
  if (feed_m > 59) {
    feed_m = 22;
    feed_duration = 20;
  }
  EEPROM.end();
}


bool feeding_done = false;
void loop() {
  
#ifdef MEM_DEBUG  
  Serial.println("Memory before: "+String(ESP.getFreeHeap()));
#endif  

 /* ========== main screen ============ */
 read_hw_clock();
 draw_main_screen();
 Serial.print("Feeding done: ");
 Serial.println(feeding_done);

 /* ========== feed logic ============ */
 int current_minute = now_h*60+now_m;
 int feed_minute = feed_h*60+feed_m;

 Serial.print("Feeding timestamp: ");
 Serial.print(feed_minute);
 Serial.print(" current timestamp: ");
 Serial.println(current_minute);
 
 // feeding_done is not persisted in nv memory, so power outage after feeding time
 // will result in feeding again 
 if (current_minute>=feed_minute) { //>= in case we had power outage and feed time already passed
  if (!feeding_done){
    feed();
    feeding_done = true;
  }
 }

 if (current_minute == 0) { // new day new feeding is due
   feeding_done = false;
 }

 digitalWrite(BUILTIN_LED, HIGH);  // turn off onboard LED

 
 /* ========== menu ============ */
 if (u8g2.getMenuEvent()==U8X8_MSG_GPIO_MENU_SELECT) {
    u8g2.setFont(u8g2_font_6x10_tf);
    u8g2.clearBuffer();   
    u8g2.setFontRefHeightAll();  
    if ( 2 == u8g2.userInterfaceMessage("Unlock?", 0, 0, " No \n Yes ") ) {
      menu_pos = u8g2.userInterfaceSelectionList("Main", menu_pos, MENU_POSITIONS);
      switch (menu_pos) {
        case MP_FEED:
          if (u8g2.userInterfaceMessage("Feed now?", 0, 0, " No \n Yes ") == 2) {
            feed();
            Serial.print("Feeding!");
          }
          break;
        case MP_SET_CLOCK: // current time setting 
          set_time_helper("Clock", &now_h, &now_m);
          Clock.setMinute(now_m);
          Clock.setHour(now_h);
          break;
        case MP_SET_TIME: // feed time setting
          set_time_helper("Feed", &feed_h, &feed_m);
          //save programmed feed time
          set_feed_time();
          break;
        case MP_SET_DOSE:
            u8g2.userInterfaceInputValue("Select\ndose", "t= ", &feed_duration, 1, 99, 2, "00 ms");        
            set_feed_time();
          break;
        case MP_EXIT:
          break;          
        default:
          break;
      }
      get_feed_time();      
   }

 }
}

void servo_cw() {
   myservo.attach(SERVO_PIN);
   myservo.write(180);
}

void servo_stop() {
   myservo.write(0);  
   myservo.detach();
}


void setup() {
  Serial.begin(115200); 
  Serial.print("compiled: ");
  Serial.print(__DATE__);
  Serial.println(__TIME__);  
#ifdef USE_WIFI    
   WiFiMulti.addAP("KrasnalZeSwiebodzinaIsBack", "Kurka786Kurka");
   WiFiMulti.addAP("SMC2", "Kurka786");
   WiFiMulti.addAP("Livebox-03FC","E4A9AC901991FFA74A9EFF7C5C");
   WiFiMulti.addAP("SmolenskForever","LECHU666");
   WiFiMulti.addAP("TuByuemZweimal","JasnaKurwaI100Milicjantow");
   initWifi();
#endif   

   pinMode(R_BTN, INPUT_PULLUP);
   pinMode(W_BTN, INPUT_PULLUP);
   pinMode(B_BTN, INPUT_PULLUP);
   pinMode(BUILTIN_LED, OUTPUT);  
   digitalWrite(BUILTIN_LED, HIGH);  // turn off onboard LED

   // factory new DS3132 has oscillator stopped to save battery power, bring it up
   if (!Clock.oscillatorCheck()) {
      Clock.setSecond(0);
   }

   // read programmed feed time
   get_feed_time();
   
   u8g2.begin(R_BTN,B_BTN,W_BTN,U8X8_PIN_NONE, U8X8_PIN_NONE, U8X8_PIN_NONE);
   oled_setup();
}


